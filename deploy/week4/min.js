const imagemin = require('imagemin');
const imageminGifsicle = require('imagemin-gifsicle');
const imageminMozjpeg = require('imagemin-mozjpeg');
// const imageminPngquant = require('imagemin-pngquant');
 
imagemin(['./*.{jpg,png}'], './build', {
	use: [
		imageminMozjpeg(),imageminGifsicle()
	]
}).then(() => {
	console.log('images optimized');
	//=> [{data: <Buffer 89 50 4e …>, path: 'build/images/foo.jpg'}, …] 
});