var GIFEncoder = require('gif-stream/encoder');
var neuquant = require('neuquant');
var fs = require('fs');
var PNGDecoder = require('png-stream/decoder');


// or, pipe data from another RGB stream 
// boom: streaming image transcoding! 
fs.createReadStream('McDWeekends_Week4_Day1_300x250_EN.png')
  .pipe(new PNGDecoder)
  .pipe(new neuquant.Stream)
  .pipe(new GIFEncoder)
  .pipe(fs.createWriteStream('out.gif'));